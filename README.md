# soal-shift-sisop-modul-3-A12-2022

### Member
- Christhoper Marcelino Mamahit (5025201249)
- I Putu Bagus Adhi Pradana (5025201010)
- Yusron Nugroho Aji (5025201138)

## Soal 1 (Belum Selesai)
### main function

```c
int main(){
	int err;
	int iter = 0;
	char buffer1[500];
	char buffer2[500];
	int status;
	
	char *dirName[] = {
		"quotes",
		"musics"
	};

    --------

}
```

pendeklarasian variable yang sekiranya diperlukan.

```c
if(fork() == 0){
	char *argv[] = {"mkdir", "-p", "hasil", NULL};
			
	execv("/usr/bin/mkdir", argv);
}
while(wait(&status) > 0);
```
Membuat folder hasil
```c
sprintf(buffer1, "hasil/quotes.txt");
			
if(fork() == 0){
	char *argv[] = {"touch", buffer1, NULL};
			
	execv("/usr/bin/touch", argv);			
}
			
while(wait(&status) > 0);
```
Membuat file quotes.txt pada directory hasil yang sudah dibuat

```c
sprintf(buffer2, "hasil/musics.txt");
			
if(fork() == 0){
	char *argv[] = {"touch", buffer2, NULL};
			
	execv("/usr/bin/touch", argv);			
}
			
while(wait(&status) > 0);
```
Membuat file musics.txt pada directory hasil yang sudah dibuat
```c
while(iter < 2){
	err = pthread_create(&(tid[iter]), NULL, &dUn, NULL);
	if(err != 0){
		printf("\n can't create thread : [%s]", strerror(err));
	}else{
		printf("\n create thread success\n");
	}
		
	iter++;
}
	
pthread_join(tid[0], NULL);
pthread_join(tid[1], NULL);
	
```
memanggil thread pada fungsi dUn

```c
if(fork()==0){
	char *argv[] = {"zip", "-rmvqP", "mihinomenestBagus", "-r", "hasil", "hasil.zip",NULL}; 
		
	execv("/usr/bin/zip", argv);
}
while(wait(&status) > 0);
```
Mengezip, memberikan password, dan menghapus directory hasil

```c
for(int i = 0; i < 2; i++){	
	if(fork() == 0){
		char *argv[] = {"rm", "-rf", dirName[i], NULL};
	
		execv("/usr/bin/rm", argv);
	}
}
while(wait(&status) > 0);
```
Menghapus directory musics dan quotes
```c
if(fork() == 0){
	char *argv[] = {"unzip", "-qP", "mihinomenestBagus", "-r", "hasil.zip",  NULL};
			
	execv("/usr/bin/unzip", argv);
}
	
while(wait(&status) > 0);
	
```
Pengunzipan file hasil.zip yang sudah dibuat sebelumnya untuk menambahkan file no.txt

```c
char buff[500];
	
sprintf(buff, "hasil/no.txt");
			
if(fork() == 0){
	char *argv[] = {"touch", buff, NULL};
			
	execv("/usr/bin/touch", argv);			
}
	
while(wait(&status) > 0);
```
Pembuatan file no.txt pada directory hasil

```c
FILE *fptr;
fptr = fopen(buff, "a+");
	
fprintf(fptr, "No");
fclose(fptr);
```

Penulisan kata No pada file no.txt

```c
if(fork()==0){
	char *argv[] = {"zip", "-rmvqP", "mihinomenestBagus", "-r", "hasil", "hasil", NULL}; 
		
	execv("/usr/bin/zip", argv);
}
	
while(wait(&status) > 0);
```
Zip ulang file hasil yang baru dengan password yang sama dengan sebelumnya

### dUn Function
```c
void* dUn(void *arg){
	int iter;
	unsigned long i = 0;
	pthread_t id = pthread_self();
	
	int status;
	
	char *link[] = {
		"https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download",
		"https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"
	};
	
	char *name[] = {
		"quotes.zip",
		"musics.zip"
	};
	
	char *dirName[] = {
		"quotes",
		"musics"
	};


    -----

}
```
Pendeklarasian variable yang diperlukan

```c
if(pthread_equal(id, tid[0])){
  
  -----
  
}
```
Thread pertama mengurus quotes

Komponen thread pertama
```c
if(fork() == 0){
	char *argv[] = {"mkdir", "-p", dirName[0], NULL};
			
	execv("/usr/bin/mkdir", argv);
}
while (wait(&status) > 0);
```
Membuat directory quotes
```c
if(fork() == 0){
	char *argv[] = {"wget", "-q", "--no-check-certificate", link[0], "-O", name[0], NULL};
			
	execv("/usr/bin/wget", argv);
}
		
while (wait(&status) > 0);
```
Mendownload file zip
```c		
if(fork() == 0){
	char *argv[] = {"unzip", name[0], "-d", "./quotes", NULL};
			
	execv("/usr/bin/unzip", argv);
}
		
while (wait(&status) > 0);
```
Unzip file yang telah didownload
```c		
if(fork() == 0){
	char *argv[] = {"rm", name[0], NULL};
			
	execv("/usr/bin/rm", argv);
}
		
while (wait(&status) > 0);
```
Hapus file zip

```c
if(fork() == 0){
	for(int i = 1; i <= 9; i++){
		char code1[100];
		char buff[500];
		
		sprintf(buff, "quotes/q%d.txt", i);
		
		FILE *fptr;
		fptr = fopen(buff, "a+");
	
		fscanf(fptr, "%s", code1);
		fclose(fptr);
	
		fptr = fopen("./hasil/quotes.txt", "a+");
	
		long decode_size1 = strlen(code1);
    	char * decoded_data1 = base64_decode(code1, decode_size1, &decode_size1);
    	
    	fprintf(fptr, "%s\n",decoded_data1);
    	fclose(fptr);
    		
    	memset(code1, 0, sizeof code1);
    }
}
while (wait(&status) > 0);
```
Pembaca file txt dari file yang sudah didownload dan penulisan pada file txt baru yaitu quotes.txt

```c
else if(pthread_equal(id, tid[1])){

    ------

}
```
Thread untuk mengurus musics

```c
if(fork() == 0){
	char *argv[] = {"mkdir", "-p", dirName[1], NULL};
			
	execv("/usr/bin/mkdir", argv);
}

while (wait(&status) > 0);
```
Membuat directory musics
```c
if(fork() == 0){
	char *argv[] = {"wget", "-q", "--no-check-certificate", link[1], "-O", name[1], NULL};
			
	execv("/usr/bin/wget", argv);
}
		
while (wait(&status) > 0);
```
Mendownload file zip
```c
if(fork() == 0){
	char *argv[] = {"unzip", name[1], "-d", "./musics",  NULL};
			
	execv("/usr/bin/unzip", argv);
}
		
while (wait(&status) > 0);
```
Unzip file yang telah didownload
```c		
if(fork() == 0){
	char *argv[] = {"rm", name[1], NULL};
			
	execv("/usr/bin/rm", argv);
}
		
while (wait(&status) > 0);
```
Hapus file zip
```c
if(fork() == 0){
	for(int i = 1; i <= 9; i++){
		char code2[100];
		char buff[500];
    		
    	sprintf(buff, "musics/m%d.txt", i);
    		
		FILE *fptr;
		fptr = fopen(buff, "a+");
	
		fscanf(fptr, "%s", code2);
		fclose(fptr);
	
		fptr = fopen("./hasil/musics.txt", "a+");
	
		long decode_size2 = strlen(code2);
    	char * decoded_data2 = base64_decode(code2, decode_size2, &decode_size2);
    	
    	fprintf(fptr, "%s\n",decoded_data2);
    	fclose(fptr);
    		
    	memset(code2, 0, sizeof code2);
    }
}
while (wait(&status) > 0);
```
Pembaca file txt dari file yang sudah didownload dan penulisan pada file txt baru yaitu musics.txt

```c
static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};
```
Pendeklarasian beberapa variable umum

```c
void build_decoding_table() {
	decoding_table = malloc(256);
    	for (int i = 0; i < 64; i++){
        	decoding_table[(unsigned char) encoding_table[i]] = i;
	}
}
```
Membuat tabel decoding

```c
void base64_cleanup() {
    	free(decoding_table);
}
```
Membersihkan tabel decoding
```c
char *base64_encode(const unsigned char *data, size_t input_length, size_t *output_length) {
    	*output_length = 4 * ((input_length + 2) / 3);
    	char *encoded_data = malloc(*output_length);
    	
    	if (encoded_data == NULL) return NULL;
    	
    	for (int i = 0, j = 0; i < input_length;) {
        	u_int32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
        	u_int32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
        	u_int32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;
        	u_int32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;
        	
        	encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        	encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        	encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        	encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    	}
    
    	for (int i = 0; i < mod_table[input_length % 3]; i++){
        	encoded_data[*output_length - 1 - i] = '=';
    	}
    	
    	return encoded_data;
}
```
Fungsi untuk menencoding data

```c

unsigned char *base64_decode(const char *data, size_t input_length, size_t *output_length) {
    	if (decoding_table == NULL){
    	 	build_decoding_table();
    	}
    	
    	if (input_length % 4 != 0){ 
    		return NULL;
    	}
    	
    	*output_length = input_length / 4 * 3;
    	
    	if (data[input_length - 1] == '='){ 
    		(*output_length)--;
    	}
    	
    	if (data[input_length - 2] == '='){
    	 	(*output_length)--;
    	}
    	
    	unsigned char *decoded_data = malloc(*output_length);
    	
    	if (decoded_data == NULL) return NULL;
    	
    	for (int i = 0, j = 0; i < input_length;) {
        	u_int32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        	u_int32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        	u_int32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        	u_int32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        	u_int32_t triple = (sextet_a << 3 * 6) + (sextet_b << 2 * 6) + (sextet_c << 1 * 6) + (sextet_d << 0 * 6);
        
        	if (j < *output_length){ 
        		decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        	}
        	
        	if (j < *output_length){
        		decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        	}
        	
        	if (j < *output_length){ 
        		decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
        	}
    	}
    	
    	return decoded_data;
}

```
Decoding data

### Permasalahan dalam pengerjaan soal 1
* Pendecode yang masih belum bisa menghasilkan hasil decode yang baik

## Soal 2
### Penjelasan tentang client.c
```
struct UserSchema
{
    char name[100];
    char password[100];
} user;

bool isUserAuthenticated = false;
int authType;
char command[256];
```
Program dimulai dengan inisialisasi variabel-variabel awal di atas. `isUserAuthenticated` akan berubah menjadi true ketika user berhasil login, lalu informasi mengenai dan password akan disimpan di struct user. Terdapat juga variabel `authType` untuk mengidentifikasi apakah user ingin login atau registr dan `command` untuk menangkan input user, apakah ingin add, see, download, maupun submit.

```
while (1) {
        if (!isUserAuthenticated) {
            printf("Choose options: 1. Login | 2. Register\n");
            printf("Enter your option: ");
            scanf("%d", &authType);
            switch (authType)
            {
            case 1:
                sendMessage("login");
                loginUser(&isUserAuthenticated);
                break;
            case 2:
                sendMessage("register");
                registerUser();
                break;
            default:
                printf("Unrecognized command. Please submit appropriate option.");
                break;
            }
            if (isUserAuthenticated)
                getchar();
        }
        else {
            printf("Enter command: ");
            fgets(command, 256, stdin);
            command[strcspn(command, "\n")] = 0;
            if (strcmp(command, "add") == 0) {
                sendMessage("add");
                addProblem();
                char feedbackMessage[100] = {0};
                read(sock, feedbackMessage, 100);
                printf("\n%s\n\n", feedbackMessage);
            } else if (strcmp(command, "see") == 0) {
                sendMessage("see");
                readProblemList();
            } else if (strstr(command, "download")) {
                sendMessage("download");
                downloadProblem(command);
            } else if (strstr(command, "submit")) {
                printf("masuk submit\n");
                sendMessage("submit");
                submitAnswer(command);  
            } else if (strcmp(command, "exit") == 0) {
                sendMessage("exit");
                break;
            } else {
                printf("\nUnrecognized command. Please submit appropriate option.\n\n");
            }
        }
    }

    printf("\nClient disconnnected.\n");
```
Client akan tetap running dikarenakan berjalan pada loop while di atas. Terdapat dua bagian besar, yaitu ketika user belum terautentikasi dan sudah terautentikasi.

Jika user belum terautentikasi, maka tersedia opsi login dan register saja. Client akan mengirimkan string login atau register kepada server untuk identifikasi logic selanjutnya diikuti fungsi yang terkait, yaitu `loginUser(&isUserAuthenticated)` atau  `registerUser()`.
Login user membawa variabel isUserAuthenticated untuk diubah nilainya menjadi true ketika login berhasil.

Jika user sudah terautentikasi, maka tersedia pengkondisian sesuai command yang masukkan user, apakah itu add, see, download, dan submit. Tiap pengkondisian akan lanjut ke fungsinya masing-masing, seperti `addProblem()`, `readProblemList`, dan lainnya.

Untuk keluar dari program, user cukup meng-input `exit`, maka program akan selesai dengan ditandai print "Client disconnnected".

```
void loginUser(bool *isUserAuthenticated) {
    char id[100], password[100];
    printf("Enter id: ");
    scanf("%s", id);
    printf("Enter password: ");
    scanf("%s", password);

    sendMessage(id);
    sendMessage(password);

    char isLoginSuccess[10] = {0};
    read(sock, isLoginSuccess, 10);

    if (strcmp(isLoginSuccess, "true") == 0) {
        *isUserAuthenticated = true;
        strcpy(user.name, id);
        strcpy(user.password, password);
    }

    char feedbackMessage[100] = {0};
    read(sock, feedbackMessage, 100);
    printf("\n%s\n\n", feedbackMessage);
}
```
Fungsi loginUser di atas berperan dalam proses login user. Logic-nya adalah me-scan input id dan password dari user, lalu dikirimkan ke server. Setelah itu, client akan menerima feedback dari server ditandai dengan variabel `isLoginSuccess` yang bernilai array char true atau false. Jika true, maka variabel `isUserAuthenticated` diubah menjadi true dan informasi id serta password user diisikan.
Di akhir, client juga akan menerima `feedbackMessage` dari server apakah login sukses maupun pesan error jika ada kesalahan.

```
void registerUser() {
    char id[100], password[100];
    printf("Enter id: ");
    scanf("%s", id);
    printf("Enter password: ");
    scanf("%s", password);

    sendMessage(id);
    sendMessage(password);

    char feedbackMessage[100] = {0};
    read(sock, feedbackMessage, 100);

    printf("\n%s\n\n", feedbackMessage);
}
```
Fungsi registerUser di atas berperan dalam proses registrasi user. Logic-nya dimulai dengan me-scan id dan password dari user, lalu mengirimkannya ke server. Setelah itu, client akan menerima pesan feedback dari server apakah user berhasil didaftarkan atau terjadi kesalahan.

```
void addProblem() {
    for (int i = 0; i < 4; i++)
    {
        char question[100] = {0}, answer[100] = {0};
        read(sock, question, 100);
        printf("%s", question);
        scanf("%s", answer);
        if (i != 0)
            sendProblemContent(answer);
        else
        {
            sendMessage(answer);
            char isProblemAlreadyExsists[17] = {0};
            read(sock, isProblemAlreadyExsists, 17);

            if (strcmp(isProblemAlreadyExsists, "problem-exists-1") == 0)
            {
                break;
            }
        }
    }
    getchar();

}
```
Fungsi addProblem di atas berfungsi ketika user ingin menambahkan problem melalui command add. Client akan meminta 4 input (4 for loop), yaitu, judul problem, path file deskripsi, path file input, dan path file output yang lalu dikirimkan ke server. Getchar() di akhir hanya digunakan untuk menangkap enter setelah scanf.
Loop pertama menandakan judul soal, maka ada pengecekan khusus apakah nama soal tersebut sudah ada atau belum. Client akan mengirimkan nama soalnya dan di akhir menerima pesan dari server. Pesannya `problem-exists-1` dari server menandakan bahwa soal sudah ada, mak program akan di-break. Selain dari nama soal, client akan mengirimkan path file deskipsi, input, dan output ke server untuk dibaca kontennya dan disimpan.

```
void *sendToServerAMessage(void *arg) {
    char *args = (char *)arg;
    char *message;
    int len;
    char *token = strtok(args, "|");
    strcpy(message, token);
    token = strtok(NULL, "\n");
    len = atoi(token);
    send(sock, message, len, 0);
}


void sendMessageWithFixedLength(char value[], char len[]) {
    char args[205];
    strcpy(args, value);
    strcat(args, "|");
    strcat(args, len);

    pthread_t th;
    pthread_create(&th, NULL, &sendToServerAMessage, (void *)args);
    pthread_join(th, NULL);
}


void sendProblemContent(char filePath[]) {
    FILE *fp;
    fp = fopen(filePath, "r");
    char str[200];

    while (fgets(str, 200, fp) != NULL) {
        sendMessageWithFixedLength(str, "200");
    }

    sendMessageWithFixedLength("stop-add-file 123", "200");

    fclose(fp);
}
```
Fungsi sendProblemContent berperan untuk mengirimkan path file deskripsi, input, dan output soal terkait yang ditambahkan oleh user. Fungsi ini dibuat untuk mengatasi kesalahan baca dari server terkait panjang pesan yang diteerima. Fungsi ini melampikan panjangnya pesan melalui fungsi `sendMessageWithFixedLength()` dan `sendToServerAMessage()` sehingga lamanya pembacaan di server tidak melampaui konten pesan sebenarnya.

```
void readProblemList() {
    puts("");
    while (true) {
        char problemDetail[200] = {0};
        read(sock, problemDetail, 200);

        if (strcmp(problemDetail, "stop") == 0)
            break;
        printf("%s\n", problemDetail);
    }
    puts("");
}
```
Fungsi readProblemList berperan untuk mengembalikan semua daftar problems yang terdaftar di file `problems.tsv` di server. Variabel `problemDetail` akan menyimpan format string judul problem dan author-nya. Fungsi akan berhenti ketika server mengirimkan stop yang artinya semua daftar problem sudah habis.

```
void submitAnswer(char command[]) {
     char judul[100], answerFile[100];
    char *token = strtok(command, " ");

    token = strtok(NULL, " ");
    strcpy(judul, token);
    sendMessage(judul);
    token = strtok(NULL, "\n");
    strcpy(answerFile, token);
    sendMessage(answerFile);

    char verdict[10] = {0};

    read(sock, verdict, 10);
    printf("\n%s\n\n", verdict);

}
```
Fungsi submitAnswer berperan untuk submit output oleh user. Fungsi ini akan mengirimkan judul problem dan path file output milik user. Variabel command berisikan `submit <judul> <pathfileoutput>` sehingga diperlukan strtok untuk mengambil judul dan path filenya. Di akhir, fungsi akan menerima verdict dari server, apakah AC atau WA. 

```
void downloadProblem(char command[]) {
    char judul[100];

    char *token = strtok(command, " ");
    token = strtok(NULL, "\n");

    strcpy(judul, token);
    sendMessage(judul);

    char descPath[100] = "./", inputPath[100] = "./";
    strcat(descPath, judul);
    strcat(descPath, "/description.txt");
    strcat(inputPath, judul);
    strcat(inputPath, "/input.txt");

    mkdir(judul, 0777);

    saveProblemFile(descPath);
    saveProblemFile(inputPath);

    printf("\nProblem %s has been successfully downloaded.\n\n", judul);
}
```
Fungsi downloadProblem berperan untuk mengatasi mendownload soal dari server ke client. Fungsi ini akan mengirim judul soal yang diinput user ke server untuk disiapkan kontennya. Setelah itu, client akan membuat folder sesuatu judul soal dan menerima konten soal di fungsi `saveProblemFile()`.

```
void saveProblemFile(char filePath[]) {
    FILE *fptr;

    fptr = fopen(filePath, "w");
    if (fptr == NULL) {
        perror(filePath);
        exit(0);
    }

    char str[100] = {0};

    while (true) {
        read(sock, str, 100);

        if (strcmp(str, "file-stop 123") == 0)
            break;

        fprintf(fptr, "%s", str);
        memset(str, 0, 100);
    }

    fclose(fptr);
}
```
Fungsi saveProblemFile berperan untuk menerima konten soal dari server. Fungsi ini akan membuat file `description.txt` dan `input.txt` dengan method `fopen`. Lalu, fungsi ini akan terus membaca pesan dari server, yaitu konten file terkait dan diakhiri dengan pesan `file-stop 123` dari server yang menandakan bahwa pembacaan file sudah selesai.

### Penjelasan tentang server.c
```
struct UserSchema
{
    char name[100];
    char password[100];
} user;

FILE *fp;
if (access("problems.tsv", F_OK) != 0)
{
    fp = fopen("problems.tsv", "w+");
    fclose(fp);
}
if (access("users.txt", F_OK) != 0)
{
    fp = fopen("users.txt", "w+");
    fclose(fp);
}

bool isUserAuthenticated = false;    
```
Terdapat juga struct user di server untuk menyimpan kredensial user. Di awal saat server connect dengan client, akan dilakukan pembuatan file `problems.tsv` dan `users.txt` jika belum ada serta inisialisasi variabel `isUserAuthenticated`.

```
restartPoint:
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

// another code

printf("Server successfully connected to client...\n\n");

close(server_fd);

// another code

while (1) {
        char command[10] = {0};
        if (!isUserAuthenticated) {
            read(new_socket, command, 10);
            if (strcmp(command, "login") == 0) {
                loginUser(&isUserAuthenticated);
            } else if (strcmp(command, "register") == 0) {
                registerUser();
            }
        } else {
            read(new_socket, command, 10);

            if (strcmp(command, "add") == 0) {
                char judul[100] = {0}, descPath[100] = {0}, inputPath[100] = {0}, outputPath[100] = {0};
                sendMessage("Judul problem: ");
                read(new_socket, judul, 100);
                sendMessage("Filepath description.txt: ");
                read(new_socket, descPath, 100);
                sendMessage("Filepath input.txt: ");
                read(new_socket, inputPath, 100);
                sendMessage("Filepath output.txt: ");
                read(new_socket, outputPath, 100);
                addProblem(judul, descPath, inputPath, outputPath);
            } else if (strcmp(command, "see") == 0) {
                seeAllProblems();
            } else if (strcmp(command, "download") == 0)
                downloadProblem();
            }
            else if (strcmp(command, "submit") == 0) {
                submitAnswer();
            }
            else if (strcmp(command, "exit") == 0)
                break;
        }
    }

    printf("\nServer is restarting...\n");

    goto restartPoint;

```
Server juga tetap running dikarenakan while loop. Terdapat juga dua bagian besar, yaitu ketika user belum terautentikasi dan sudah terautentikasi.
Tepat setelah sebuah client terkoneksi dengan server, maka fungsi `close(server_fd)` diekseskusi untuk menutup koneksi yang akan datang. Untuk keluar, client akan mengirimkan pesan `exit` yang akan mem-break program dan print `Server is restarting...` karena terdapat `goto` setelahnya yang membawa program kembali ke atas sebelum koneksi terjadi sehingga variabel socket tidak dibuat baru tapi di-restart.
Dengan demikian, hanya ada satu client yang bisa terkoneksi ke server dalam satu waktu

```
void loginUser(bool *isUserAuthenticated) {
    char id[100] = {0}, password[100] = {0};
    read(new_socket, id, 100);
    read(new_socket, password, 100);

    if (isUserExists(id, password))
        *isUserAuthenticated = true;

    if (*isUserAuthenticated) {
        strcpy(user.name, id);
        strcpy(user.password, password);
        sendMessage("true");
        sendMessage("User has successfully logged in.");
    } else {
        sendMessage("false");
        sendMessage("Failed to log in. User does not exists. Please try again.");
    }
}
```
Di loginUser, server akan menerima id dan password dari client. Lalu dicek apakah user bersangkutan ada pada fungsi `isUserExists()`. Jika login berhasil, `isUserAuthenticated` akan menjadi true dan struct user akan diisikan. Langkah terakhir adalah mengirimkan pesan feedback ke client apaah login berhasil atau tidak dengan pengkondisian berdasarkan variabel `isUserAuthenticated`.

```
void registerUser() {
    char id[100] = {0}, password[100] = {0};
    read(new_socket, id, 100);
    read(new_socket, password, 100);

    if (!isIdUnique(id)) {
        sendMessage("The id has been registered. Please use another name.");
        return;
    }

    if (!isPasswordValid(password)) {
        sendMessage("Password must contain a number, a lowercase, an uppercase, and consist of minimum 6 characters.");
        return;
    }

    registerNewUser(id, password);
    sendMessage("User has successfully registered.");
}
```
Logic pada registrasi user mirip seperti login, namun terdapat dua validasi yaitu id keunikan dan format passwordnya. Server akan mengirimkan pesan feedback juga apakah registrasi berhasil maupun jika ada kesalahan.

```
bool isIdUnique(char id[]) {
    FILE *fp;
    fp = fopen("users.txt", "r");

    char str[100];
    while (fgets(str, 100, fp) != NULL) {
        char *token = strtok(str, ":");
        if (strcmp(token, id) == 0)
        {
            fclose(fp);
            return false;
        }
    }

    fclose(fp);
    return true;
}
```
Fungsi isIdUnique memiliki return type boolean dan berperan dalam memeriksa apakah id untuk registrasi masih unik atau tidak. Caranya adalah dengan membaca file `users.txt`. Lalu, dengan strtok, kita akan mengekstrak nilai id-nya. Jika id terdaftar ada yang sama dengan id inputan user, maka unik tidak unik dan returnnya false. Jika sampai akhir tidak ditemukan id yang sama, return true.

```
bool isPasswordValid(char password[]) {
    int len = strlen(password);
    if (len < 6)
        return false;

    bool number, lowercase, uppercase = false;
    for (int i = 0; i < len; i++) {
        if (password[i] >= 97 && password[i] <= 122)
            lowercase = true;
        else if (password[i] >= 65 && password[i] <= 90)
            uppercase = true;
        else if (password[i] >= 48 && password[i] <= 57)
            number = true;
    }

    if (number && lowercase && uppercase)
        return true;
    return false;
}
```
Fungsi isPasswordValid memiliki return type boolean dan  berperan untuk memvalidasi format password user. Dengan strlen, kita cek panjang char array dan harus lebih dari 6. Setelah itu, diperiksa juga jenis kakrakternya dengan variabel angka, lowercase, dan uppercase dengan for loop. Variabel tersebut akan diubah menjadi true jika nilai ascii nya sesuai. Di akhir, jika angka, lowercase, dan uppercase terpenuhi semua, fungsi akan me-return-kan true, sebaliknya false.

```
void registerNewUser(char id[], char password[]) {
    FILE *fp;
    fp = fopen("users.txt", "a");

    char credentials[200];
    strcpy(credentials, id);
    strcat(credentials, ":");
    strcat(credentials, password);
    strcat(credentials, "\n");

    fputs(credentials, fp);

    fclose(fp);
};
```
Jika validasi id dan password sudah lolos semua, maka registrasi akan dilakukan di fungsi `registerNewUser()` ini. Fungsi ini akan meng-append ke file `users.txt`, yaitu id dan password nya yang dipisah dengan titik dua (:).

```
bool isProblemNameAlreadyExists(char judul[]) {
    FILE *fp;
    fp = fopen("problems.tsv", "r");

    char str[200];

    while (fgets(str, 200, fp) != NULL)
    {
        char *token = strtok(str, "\t");

        if (strcmp(token, judul) == 0)
            return true;
    }

    fclose(fp);
    return false;
}


void addProblem() {
    char judul[100] = {0};
    sendMessage("Judul problem: ");
    read(new_socket, judul, 100);

    if (isProblemNameAlreadyExists(judul))
    {
        sendMessage("problem-exists-1");
        sendMessage("Process failed. Problem already exists. Please try with another name.");
        return;
    }
    else
        sendMessage("problem-exists-0");

    addProblemList(judul);
    makeDirectory(judul);

    sendMessage("Filepath description.txt: ");
    makeAndCopyFileContent(judul, "description.txt");

    sendMessage("Filepath input.txt: ");
    makeAndCopyFileContent(judul, "input.txt");

    sendMessage("Filepath output.txt: ");
    makeAndCopyFileContent(judul, "output.txt");

    sendMessage("Problem has been successfully added.");

}
```
Fungsi addProblem berperan dalam menambahkan problem baru. Server menerima judul, file path deskripsi, file path input, file path output dari client. 
Pertama, fungsi ini akan mengecek apakah nama problem sudah terdaftar melalui fungsi `isProblemNameAlreadyExists()` yang membandingkan nama soal dengan daftar di file `problems.tsv`.
Setelah itu, fungsi akan mencatat judul dan author soal ke dalam file `problems.tsv` dengan fputs.
Lalu, fungsi akan membuat directory berdasarkan judul yang diterima melalui fungsi custom `makeDirectory()`.
Lalu 3 file tadi, yaitu deskripsi, input, dan output akan dicopy ke directory judul tadi.
Di akhir, akan dikirim pesan feedback bahwa proses berhasil.

```
void seeAllProblems() {
    FILE *fp;
    fp = fopen("problems.tsv", "r");

    char str[200];

    while (fgets(str, 200, fp) != NULL) {
        char problemDetail[200];

        char *token = strtok(str, "\t");
        strcpy(problemDetail, token);
        strcat(problemDetail, " by ");
        token = strtok(NULL, "\n");
        strcat(problemDetail, token);

        sendMessage(problemDetail);
    }
    sendMessage("stop");

    fclose(fp);
}
```
Fungsi `seeAllProblems` berperan untuk mengatasi command see dari user, yaitu menampilkan semua daftar problems yang ada. Caranya adalah fungsi akan membuka file `problems.tsv`, lalu dibaca dengan fgets dan strtok akan mengekstrak nilai judul serta author-nya. Nilai tersebut dikirimkan kembali ke client. Di akhir, server akan mengirimkan string "stop" yang menandakan proses sudah selesai.

```
void submitAnswer() {
    char judul[100] = {0}, answerFile[100] = {0};

    read(new_socket, judul, 100);
    read(new_socket, answerFile, 100);

    if (isFilesEqual(judul, answerFile))
        sendMessage("AC");
    else
        sendMessage("WA");
}
```
Fungsi submitAnswer berperan untuk mengatasi command submit dari user. Fungsi akan membaca judul dan path file output dari client. Lalu, perbandingan akan diproses oleh fungsi `isFilesEqual()`. Jika file sama, server akan mengirimkan pesan feedback "AC", sebaliknya "WA".

```
bool isFilesEqual(char judul[], char answerFile[]) {
    FILE *fp1, *fp2;

    char realAnswerFile[100];
    strcpy(realAnswerFile, judul);
    strcat(realAnswerFile, "/output.txt");

    fp1 = fopen(realAnswerFile, "r");
    fp2 = fopen(answerFile, "r");

    if (fp1 == NULL) {
        perror(realAnswerFile);
        exit(0);
    }
    if (fp2 == NULL) {
        perror(answerFile);
        exit(0);
    }

    char c1 = fgetc(fp1);
    char c2 = fgetc(fp2);

    while (c1 != EOF && c2 != EOF) {
        if (c1 != c2)
            return false;
        c1 = fgetc(fp1);
        c2 = fgetc(fp2);
    }

    fclose(fp1);
    fclose(fp2);
    return true;
}
```
Fungsi isFilesEqual memiliki return type boolean dan akan mengecek apakah nilai kedua file sama. Fungsi akan menerima dua path file akan dibandingkan, lalu dibuka dengan fopen. Tiap karakter akan dibaca dengan fgetc, jika ada yang tidak sama, maka fungsi akan return false. Sebaliknya, jika sampai akhir tidak ditemukan perbedaan, fungsi akan return true.

```
void *sendProblemContent(void *arg) {
    char *filePath = (char *)arg;

    FILE *fp;
    fp = fopen(filePath, "r");

    char str[100];

    while (fgets(str, 100, fp) != NULL) {
        sendMessage(str);
    }

    sendMessage("file-stop 123"); // mark the file reading is done

    fclose(fp);
}

void sendProblem(char filePath[]) {
    pthread_t th;
    pthread_create(&th, NULL, &sendProblemContent, (void *)filePath);
    pthread_join(th, NULL);
}

void downloadProblem() {
    char judul[100] = {0};
    read(new_socket, judul, 100);

    char descPath[100] = "./", inputPath[100] = "./";
    strcat(descPath, judul);
    strcat(descPath, "/description.txt");
    strcat(inputPath, judul);
    strcat(inputPath, "/input.txt");

    sendProblem(descPath);
    sendProblem(inputPath);
}
```
Fungsi downloadProblem berperan menangani download file oleh user. FUngsi ini menerima judul file dari server, lalu melalui fungsi `sendProblem()`, server akan mengirimkan konten file deskripsi dan inputnya menggunakan thread ke client.

### Penjelasan tentang Fungsi Utilitas
```
void *sendToServer(void *arg) {
    char *message = (char *)arg;
    send(sock, message, strlen(message), 0);
}

void sendMessage(char value[]) {
    pthread_t th;
    pthread_create(&th, NULL, &sendToServer, (void *)value);
    pthread_join(th, NULL);
}
```
Fungsi sendMessage berperan untuk mengirimkan pesan ke server dan sebaliknya. Cukup dengan passing string ke fungsi `sendMessage`, thread akan dibuatkan untuk menjalankan fungsi `sendToServer` atau `sendToClient` yang akan melakukan send socket dan membawa string yang di-passing sebelumnya beserta panjang stringnya dengan strlen.

```
void *executeMakeDirectory(void *arg) {
    char *filePath = (char *)arg;
    mkdir(filePath, 0777);
}

void makeDirectory(char filePath[]) {
    pthread_t th;
    pthread_create(&th, NULL, &executeMakeDirectory, (void *)filePath);
    pthread_join(th, NULL);
}
```
Fungsi `makeDirectory` digunakan untuk membuat sebuah folder. Fungsi ini menggunakan thread yang akan menjalankan mkdir untuk membuat folder sebuah path yang di-passing.

```
void makeAndCopyFileContent(char judul[], char fileName[]) {
    char filePath[200];
    strcpy(filePath, judul);
    strcat(filePath, "/");
    strcat(filePath, fileName);

    FILE *fptr;

    fptr = fopen(filePath, "w+");
    if (fptr == NULL) {
        perror(filePath);
        exit(0);
    }

    while (true) {
        char str[200] = {0};
        read(new_socket, str, 200);
        if (strcmp(str, "stop-add-file 123") == 0)
            break;
        fprintf(fptr, "%s", str);
    }

    fclose(fptr);
}

```
Fungsi makeAndCopyFileContent berperan untuk meng-copy sebuah file ke path lain. Fungsi ini menerima source (path file awal), dirTarget (path yang dituju), fileName (nama file yang akan disimpan nanti). Fungsi ini akan membaca konten file yang dikirim dari client. Akhuir file ditandai dengan pesan `stop-add-file 123` dari client.

## Soal 3

```c
int main(int argc, char *argv[]) {
 
    listFilesRecursively("/home/yusron/shift3/hartakarun");
 
    return 0;
}
```
Di dalam fungsi main hanya terdapat fungsi dari listFilesRecursively yang digunakan untuk melakukan proses pengakategorian file pada working directory secara rekursif

```c
void listFilesRecursively(char *basePath)
{
    char path_src[1000]; 
    struct dirent *dp; 
    struct stat statbuf; 
    pthread_t t_id;
    DIR *dir = opendir(basePath); 
    char tempat[3000];
 
    if(!dir)
        return; 
 
    while((dp = readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name,".")!=0 && strcmp(dp->d_name, "..")!=0) 
        {
 
            strcpy(path_src, basePath); 
            strcat(path_src, "/"); 
 
            
            if (dp->d_type == 4) //cek ini directory atau bukan
            {
            	rmdir(path_src);
            }
            else {
            	sprintf(tempat, "%s%s", path_src, dp->d_name);
            	pthread_create(&t_id, NULL, &auto_check, (void *)tempat);
            	pthread_join(t_id, NULL);
            	listFilesRecursively(path_src);
            }
            
        }
    }
    closedir(dir);
}
```
Fungsi listFilesRecursively di atas digunakan untuk melist file secara rekursif dengan cara program membuat thread untuk melakukan pemeriksaan atas sebuah ekstensi dari file dan membuat folder sesuai dengan ekstensi file yang ada


```c
void stringCase(char *s) {
    int i=0;
    while(s[i]!='\0') {
        if(s[i]>='A' && s[i]<='Z') {
            s[i]=s[i]+32;
        }
    i++;
    }
}
```
Fungsi ini digunakan untuk melakukan pengubahan terhadap ekstensi file menjadi lower case untuk membuat program tidak case sensitive

```c
char *get_filename_ext(char *filename) {
    struct stat st;
    char *dot = strrchr(filename, '.');

    if (hidden(filename)) return "Hidden";
    if(!dot || dot == filename) return "Unknown";

    else return dot+1;
}
```
Fungsi diatas digunakn untuk mendapatkan ekstensi file, seperti file yang tidak diketahui seperti tanda . didepan nama file akan di return Hidden dan jika tidak ada akan di return Unknown

```c
int hidden(const char *name)
{
    return name[0] == '.' && strcmp(name, ".")!=0 && strcmp(name,"..") !=0;
}
```
Sedangkan untuk fungsi hidden digunakan untuk dapat menemukan file hidden atau tersembunyi dengan name[0] == '.' && strcmp(name, ".")!=0 && strcmp(name,"..")

```c
void *auto_check(void *args) {
char path_des[1000];
    char *path_src = (char*)malloc(sizeof(char));
    path_src = (char*)args;
    char *bname = basename(path_src);
    char *ext =strdup(get_filename_ext(bname));
    //printf("%s\n", bname);
    stringCase(ext);
    if(strcmp(ext,"gz")== 0) {
        ext = "tar.gz";
    }
    strcpy(path_des, "/home/yusron/shift3/hartakarun");
    strcat(path_des, "/");
    strcat(path_des, ext);
    strcat(path_des, "/");
    create_dir(path_des);
    strcat(path_des, bname);
    rename(path_src, path_des);
}
```
Merupakan sebuah function untuk pengaktegorian file menggunakan thread, kemudian peran thread disini digunakan untuk membuat directory baru sesuai dengan ekstensi file yang ada.


```c
child_id = fork();
	if(child_id < 0)
 	{
		exit(EXIT_FAILURE);
	}

	if(child_id == 0)
	{
		char *argv[] = {"zip", "-r", "/home/yusron/Client/hartakarun.zip", "/home/yusron/shift3/hartakarun", NULL};
		if(execvp("/bin/zip", argv) == -1) {
			perror("zip failed!");
			exit(EXIT_FAILURE);
		}
	}

```
Untuk melakukan zip folder hartakarun, maka dapat digunakan fungsi execv() dengan terlebih dahulu memanggil sebuah proses baru dengan fungsi fork().
Auntuk disimpan ke working directory dari client, maka sebuah directory baru bernama Client akan dibuat dengan bantuan mkdir

```c
scanf("%[^\n]s", command);
	if(strcmp(command, "send hartakarun.zip") != 0) {
		printf("Invalid command, exiting program");
		return 0;
	}

```
Untuk program client, untuk mengirimkan file hartakarun.zip, client harus terlebih dahulu menuliskan command "send hartakarun.zip"


### Permasalahan dalam pengerjaan soal 3

permasalahan dapat diselesaikan dengan mengguakan unzip manual di dalam directory folder yang telah ada, dan setelah tu dilakukan proses ekseskusi untuk dapat melanjutkan proses selanjutnya, dan menggunakan stringCase untuk membuat program tidak case sensitive, proses untuk penggunaan di soal d dan e yang menemui beberapa kendala
