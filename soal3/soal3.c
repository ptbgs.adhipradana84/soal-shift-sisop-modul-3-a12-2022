#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <dirent.h>
#include <libgen.h>
#include <pthread.h>
#include <errno.h>

// const char *prg;
 
static void create_dir(const char *dir) {
    if(mkdir(dir,0755) < 0) {
        if (errno != EEXIST) {
            perror(dir);
            exit(1);
        }
    }
}
 
void stringCase(char *s) {
    int i=0;
    while(s[i]!='\0') {
        if(s[i]>='A' && s[i]<='Z') {
            s[i]=s[i]+32;
        }
    i++;
    }
}
 
int hidden(const char *name)
{
    return name[0] == '.' && strcmp(name, ".")!=0 && strcmp(name,"..") !=0;
}
 
char *get_filename_ext(char *filename) {
    struct stat st;
    char *dot = strrchr(filename, '.');

    if (hidden(filename)) return "Hidden";
    if(!dot || dot == filename) return "Unknown";

    else return dot+1;
}
 
void *auto_check(void *args) {
char path_des[1000];
    char *path_src = (char*)malloc(sizeof(char));
    path_src = (char*)args;
    char *bname = basename(path_src);
    char *ext =strdup(get_filename_ext(bname));
    //printf("%s\n", bname);
    stringCase(ext);
    if(strcmp(ext,"gz")== 0) {
        ext = "tar.gz";
    }
    strcpy(path_des, "/home/yusron/shift3/hartakarun");
    strcat(path_des, "/");
    strcat(path_des, ext);
    strcat(path_des, "/");
    create_dir(path_des);
    strcat(path_des, bname);
    rename(path_src, path_des);
}

void listFilesRecursively(char *basePath)
{
    char path_src[1000]; 
    struct dirent *dp; 
    struct stat statbuf; 
    pthread_t t_id;
    DIR *dir = opendir(basePath); 
    char tempat[3000];
 
    if(!dir)
        return; 
 
    while((dp = readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name,".")!=0 && strcmp(dp->d_name, "..")!=0) 
        {
 
            strcpy(path_src, basePath); 
            strcat(path_src, "/"); 
 
            
            if (dp->d_type == 4) //cek ini directory atau bukan
            {
                rmdir(path_src);
            }
            else {
                sprintf(tempat, "%s%s", path_src, dp->d_name);
                pthread_create(&t_id, NULL, &auto_check, (void *)tempat);
                pthread_join(t_id, NULL);
                listFilesRecursively(path_src);
            }
            
        }
    }
    closedir(dir);
}
 
int main(int argc, char *argv[]) {
 
    listFilesRecursively("/home/yusron/shift3/hartakarun");
 
    return 0;
}
