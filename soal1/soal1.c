#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stdint.h>

pthread_t tid[2];
pid_t child;

static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};
 
void build_decoding_table() {
	decoding_table = malloc(256);
    	for (int i = 0; i < 64; i++){
        	decoding_table[(unsigned char) encoding_table[i]] = i;
	}
}

void base64_cleanup() {
    	free(decoding_table);
}

char *base64_encode(const unsigned char *data, size_t input_length, size_t *output_length) {
    	*output_length = 4 * ((input_length + 2) / 3);
    	char *encoded_data = malloc(*output_length);
    	
    	if (encoded_data == NULL) return NULL;
    	
    	for (int i = 0, j = 0; i < input_length;) {
        	u_int32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
        	u_int32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
        	u_int32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;
        	u_int32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;
        	
        	encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        	encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        	encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        	encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    	}
    
    	for (int i = 0; i < mod_table[input_length % 3]; i++){
        	encoded_data[*output_length - 1 - i] = '=';
    	}
    	
    	return encoded_data;
}

unsigned char *base64_decode(const char *data, size_t input_length, size_t *output_length) {
    	if (decoding_table == NULL){
    	 	build_decoding_table();
    	}
    	
    	if (input_length % 4 != 0){ 
    		return NULL;
    	}
    	
    	*output_length = input_length / 4 * 3;
    	
    	if (data[input_length - 1] == '='){ 
    		(*output_length)--;
    	}
    	
    	if (data[input_length - 2] == '='){
    	 	(*output_length)--;
    	}
    	
    	unsigned char *decoded_data = malloc(*output_length);
    	
    	if (decoded_data == NULL) return NULL;
    	
    	for (int i = 0, j = 0; i < input_length;) {
        	u_int32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        	u_int32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        	u_int32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        	u_int32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        	u_int32_t triple = (sextet_a << 3 * 6) + (sextet_b << 2 * 6) + (sextet_c << 1 * 6) + (sextet_d << 0 * 6);
        
        	if (j < *output_length){ 
        		decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        	}
        	
        	if (j < *output_length){
        		decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        	}
        	
        	if (j < *output_length){ 
        		decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
        	}
    	}
    	
    	return decoded_data;
}

void* dUn(void *arg){
	int iter;
	unsigned long i = 0;
	pthread_t id = pthread_self();
	
	int status;
	
	char *link[] = {
		"https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download",
		"https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"
	};
	
	char *name[] = {
		"quotes.zip",
		"musics.zip"
	};
	
	char *dirName[] = {
		"quotes",
		"musics"
	};
	
	if(pthread_equal(id, tid[0])){
		if(fork() == 0){
			char *argv[] = {"mkdir", "-p", dirName[0], NULL};
			
			execv("/usr/bin/mkdir", argv);
		}
		
		while (wait(&status) > 0);
	
		if(fork() == 0){
			char *argv[] = {"wget", "-q", "--no-check-certificate", link[0], "-O", name[0], NULL};
			
			execv("/usr/bin/wget", argv);
		}
		
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"unzip", name[0], "-d", "./quotes", NULL};
			
			execv("/usr/bin/unzip", argv);
		}
		
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"rm", name[0], NULL};
			
			execv("/usr/bin/rm", argv);
		}
		
		while (wait(&status) > 0);
		
	}else if(pthread_equal(id, tid[1])){
		if(fork() == 0){
			char *argv[] = {"mkdir", "-p", dirName[1], NULL};
			
			execv("/usr/bin/mkdir", argv);
		}
		
		while (wait(&status) > 0);
	
		if(fork() == 0){
			char *argv[] = {"wget", "-q", "--no-check-certificate", link[1], "-O", name[1], NULL};
			
			execv("/usr/bin/wget", argv);
		}
		
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"unzip", name[1], "-d", "./musics",  NULL};
			
			execv("/usr/bin/unzip", argv);
		}
		
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"rm", name[1], NULL};
			
			execv("/usr/bin/rm", argv);
		}
		
		while (wait(&status) > 0);
		
		
	}
}

int main(){
	int err;
	int iter = 0;
	char buffer1[500];
	char buffer2[500];
	int status;
	
	char *dirName[] = {
		"quotes",
		"musics"
	};
	
	if(fork() == 0){
		char *argv[] = {"mkdir", "-p", "hasil", NULL};
			
		execv("/usr/bin/mkdir", argv);
	}
	while(wait(&status) > 0);
	
	sprintf(buffer1, "hasil/quotes.txt");
			
	if(fork() == 0){
		char *argv[] = {"touch", buffer1, NULL};
			
		execv("/usr/bin/touch", argv);			
	}
			
	while(wait(&status) > 0);
	
	sprintf(buffer2, "hasil/musics.txt");
			
	if(fork() == 0){
		char *argv[] = {"touch", buffer2, NULL};
			
		execv("/usr/bin/touch", argv);			
	}
			
	while(wait(&status) > 0);

	while(iter < 2){
		err = pthread_create(&(tid[iter]), NULL, &dUn, NULL);
		if(err != 0){
			printf("\n can't create thread : [%s]", strerror(err));
		}else{
			printf("\n create thread success\n");
		}
		
		iter++;
	}
	
	pthread_join(tid[0], NULL);
	pthread_join(tid[1], NULL);
	

	
		
		for(int i = 1; i <= 9; i++){
		FILE *fptr1;
		char code1[500];
		char buffing1[500];
		
		sprintf(buffing1, "quotes/q%d.txt", i);
		
		
		fptr1 = fopen(buffing1, "a+");
	
		fscanf(fptr1, "%s", code1);
		fclose(fptr1);
	
		fptr1 = fopen("./hasil/quotes.txt", "a+");
	
		long decode_size1 = strlen(code1);
    		char * decoded_data1 = base64_decode(code1, decode_size1, &decode_size1);
    	
    		fprintf(fptr1, "%s\n",decoded_data1);
    		
    		fclose(fptr1);
    		}
    	
    	



		
		for(int i = 1; i <= 9; i++){
			FILE *fptr2;
		char code2[500];
		char buffing2[500];
    		
    		sprintf(buffing2, "musics/m%d.txt", i);
    		
		fptr2 = fopen(buffing2, "a+");
	
		fscanf(fptr2, "%s", code2);
		fclose(fptr2);
	
		fptr2 = fopen("./hasil/musics.txt", "a+");
	
		long decode_size2 = strlen(code2);
    		char * decoded_data2 = base64_decode(code2, decode_size2, &decode_size2);
    	
    		fprintf(fptr2, "%s\n",decoded_data2);
    		
    		    		fclose(fptr2);

    		}

	
		
	if(fork()==0){
		char *argv[] = {"zip", "-rmvqP", "mihinomenestBagus", "-r", "hasil", "hasil.zip",NULL}; 
		
		execv("/usr/bin/zip", argv);
	}
	
	while(wait(&status) > 0);
	
	for(int i = 0; i < 2; i++){	
	if(fork() == 0){
		char *argv[] = {"rm", "-rf", dirName[i], NULL};
	
		execv("/usr/bin/rm", argv);
	}
	while(wait(&status) > 0);
	}
		
	
	
	if(fork() == 0){
		char *argv[] = {"unzip", "-qP", "mihinomenestBagus", "-r", "hasil.zip",  NULL};
			
		execv("/usr/bin/unzip", argv);
	}
	
	while(wait(&status) > 0);
	
	char buff[500];
	
	sprintf(buff, "hasil/no.txt");
			
	if(fork() == 0){
		char *argv[] = {"touch", buff, NULL};
			
		execv("/usr/bin/touch", argv);			
	}
	
	while(wait(&status) > 0);

	FILE *fptr;
	fptr = fopen(buff, "a+");
	
	fprintf(fptr, "No");
	fclose(fptr);
	
	if(fork()==0){
		char *argv[] = {"zip", "-rmvqP", "mihinomenestBagus", "-r", "hasil", "hasil", NULL}; 
		
		execv("/usr/bin/zip", argv);
	}
	
	while(wait(&status) > 0);

	exit(0);
	return 0;
}

//gcc soal1.c -o soal1 -pthread
//./soal1
