#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <wait.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <pthread.h>
#define PORT 8080

int sock = 0;

struct UserSchema
{
    char name[100];
    char password[100];
} user;

void *sendToServer(void *arg)
{
    char *message = (char *)arg;
    send(sock, message, strlen(message), 0);
}

void sendMessage(char value[])
{
    pthread_t th;
    pthread_create(&th, NULL, &sendToServer, (void *)value);
    pthread_join(th, NULL);
}

void loginUser(bool *isUserAuthenticated)
{
    char id[100], password[100];
    printf("Enter id: ");
    scanf("%s", id);
    printf("Enter password: ");
    scanf("%s", password);

    sendMessage(id);
    sendMessage(password);

    char isLoginSuccess[10] = {0};
    read(sock, isLoginSuccess, 10);

    if (strcmp(isLoginSuccess, "true") == 0)
    {
        *isUserAuthenticated = true;
        strcpy(user.name, id);
        strcpy(user.password, password);
    }

    char feedbackMessage[100] = {0};
    read(sock, feedbackMessage, 100);
    printf("\n%s\n\n", feedbackMessage);
}

void registerUser()
{
    char id[100], password[100];
    printf("Enter id: ");
    scanf("%s", id);
    printf("Enter password: ");
    scanf("%s", password);

    sendMessage(id);
    sendMessage(password);

    char feedbackMessage[100] = {0};
    read(sock, feedbackMessage, 100);

    printf("\n%s\n\n", feedbackMessage);
}

void *sendToServerAMessage(void *arg)
{
    char *args = (char *)arg;
    char *message;
    int len;
    char *token = strtok(args, "|");
    strcpy(message, token);
    token = strtok(NULL, "\n");
    len = atoi(token);
    send(sock, message, len, 0);
}

void sendMessageWithFixedLength(char value[], char len[])
{
    char args[205];
    strcpy(args, value);
    strcat(args, "|");
    strcat(args, len);

    pthread_t th;
    pthread_create(&th, NULL, &sendToServerAMessage, (void *)args);
    pthread_join(th, NULL);
}

void sendProblemContent(char filePath[])
{
    FILE *fp;
    fp = fopen(filePath, "r");
    char str[200];

    while (fgets(str, 200, fp) != NULL)
    {
        // printf("STR = %s\n", str);
        // sendMessage(str);
        sendMessageWithFixedLength(str, "200");
    }

    sendMessageWithFixedLength("stop-add-file 123", "200");

    fclose(fp);
}

void addProblem()
{
    for (int i = 0; i < 4; i++)
    {
        char question[100] = {0}, answer[100] = {0};
        read(sock, question, 100);
        printf("%s", question);
        scanf("%s", answer);
        if (i != 0)
            sendProblemContent(answer);
        else
        {
            sendMessage(answer);
            char isProblemAlreadyExsists[17] = {0};
            read(sock, isProblemAlreadyExsists, 17);

            if (strcmp(isProblemAlreadyExsists, "problem-exists-1") == 0)
            {
                break;
            }
        }
    }
    getchar();
}

void readProblemList()
{
    puts("");
    while (true)
    {
        char problemDetail[200] = {0};
        read(sock, problemDetail, 200);

        if (strcmp(problemDetail, "stop") == 0)
            break;
        printf("%s\n", problemDetail);
    }
    puts("");
}

void submitAnswer(char command[])
{
    char judul[100], answerFile[100];
    char *token = strtok(command, " ");

    token = strtok(NULL, " ");
    strcpy(judul, token);
    sendMessage(judul);
    // sendMessageWithFixedLength(judul, "100");
    token = strtok(NULL, "\n");
    strcpy(answerFile, token);
    sendMessage(answerFile);
    // sendMessageWithFixedLength(answerFile, "100");

    char verdict[10] = {0};

    read(sock, verdict, 10);
    printf("\n%s\n\n", verdict);
}

void saveProblemFile(char filePath[])
{
    FILE *fptr;

    fptr = fopen(filePath, "w");
    if (fptr == NULL)
    {
        perror(filePath);
        exit(0);
    }

    char str[100] = {0};

    while (true)
    {
        read(sock, str, 100);

        if (strcmp(str, "file-stop 123") == 0)
            break;

        fprintf(fptr, "%s", str);
        memset(str, 0, 100);
    }

    fclose(fptr);
}

void downloadProblem(char command[])
{
    char judul[100];

    char *token = strtok(command, " ");
    token = strtok(NULL, "\n");

    strcpy(judul, token);
    sendMessage(judul);

    char descPath[100] = "./", inputPath[100] = "./";
    strcat(descPath, judul);
    strcat(descPath, "/description.txt");
    strcat(inputPath, judul);
    strcat(inputPath, "/input.txt");

    mkdir(judul, 0777);

    saveProblemFile(descPath);
    saveProblemFile(inputPath);

    printf("\nProblem %s has been successfully downloaded.\n\n", judul);
}

int main(int argc, char const *argv[])
{
    struct sockaddr_in address;
    int valread;
    struct sockaddr_in serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed. Another client is in connection. \n");
        return -1;
    }

    printf("Client successfully connected to server...\n\n");

    bool isUserAuthenticated = false;
    int authType;
    char command[256];

    while (1)
    {
        if (!isUserAuthenticated)
        {
            printf("Choose options: 1. Login | 2. Register\n");
            printf("Enter your option: ");
            scanf("%d", &authType);

            switch (authType)
            {
            case 1:
                sendMessage("login");

                loginUser(&isUserAuthenticated);

                break;
            case 2:
                sendMessage("register");

                registerUser();

                break;
            default:
                printf("Unrecognized command. Please submit appropriate option.");
                break;
            }
            if (isUserAuthenticated)
                getchar();
        }
        else
        {
            printf("Enter command: ");

            fgets(command, 256, stdin);
            command[strcspn(command, "\n")] = 0;

            if (strcmp(command, "add") == 0)
            {
                sendMessage("add");
                addProblem();

                char feedbackMessage[100] = {0};
                read(sock, feedbackMessage, 100);
                printf("\n%s\n\n", feedbackMessage);
            }
            else if (strcmp(command, "see") == 0)
            {
                sendMessage("see");

                readProblemList();
            }
            else if (strstr(command, "download"))
            {
                sendMessage("download");

                downloadProblem(command);
            }
            else if (strstr(command, "submit"))
            {
                sendMessage("submit");

                submitAnswer(command);
            }
            else if (strcmp(command, "exit") == 0)
            {
                sendMessage("exit");
                break;
            }
            else
            {
                printf("\nUnrecognized command. Please submit appropriate option.\n\n");
            }
        }
    }

    printf("\nClient disconnnected.\n");

    return 0;
}