#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <fcntl.h>
#define PORT 8080

int new_socket = 0;

struct UserSchema
{
    char name[100];
    char password[100];
} user;

void *sendToClient(void *arg)
{
    char *message = (char *)arg;
    send(new_socket, message, strlen(message), 0);
}

void sendMessage(char value[])
{
    pthread_t th;
    pthread_create(&th, NULL, &sendToClient, (void *)value);
    pthread_join(th, NULL);
}

bool isUserExists(char id[], char password[])
{
    FILE *fp;
    fp = fopen("users.txt", "r");

    char str[100];

    while (fgets(str, 100, fp) != NULL)
    {
        char *token = strtok(str, ":");
        if (strcmp(token, id) == 0)
        {
            token = strtok(NULL, "\n");

            if (strcmp(token, password) != 0)
                break;

            fclose(fp);
            return true;
        }
    }

    fclose(fp);
    return false;
}

void loginUser(bool *isUserAuthenticated)
{
    char id[100] = {0}, password[100] = {0};
    read(new_socket, id, 100);
    read(new_socket, password, 100);

    if (isUserExists(id, password))
        *isUserAuthenticated = true;

    if (*isUserAuthenticated)
    {
        strcpy(user.name, id);
        strcpy(user.password, password);
        sendMessage("true");
        sendMessage("User has successfully logged in.");
    }
    else
    {
        sendMessage("false");
        sendMessage("Failed to log in. User does not exists. Please try again.");
    }
}

void registerNewUser(char id[], char password[])
{

    FILE *fp;
    fp = fopen("users.txt", "a");

    char credentials[200];
    strcpy(credentials, id);
    strcat(credentials, ":");
    strcat(credentials, password);
    strcat(credentials, "\n");

    fputs(credentials, fp);

    fclose(fp);
};

bool isIdUnique(char id[])
{
    FILE *fp;
    fp = fopen("users.txt", "r");

    char str[100];
    while (fgets(str, 100, fp) != NULL)
    {
        char *token = strtok(str, ":");
        if (strcmp(token, id) == 0)
        {
            fclose(fp);
            return false;
        }
    }

    fclose(fp);
    return true;
}

bool isPasswordValid(char password[])
{
    int len = strlen(password);

    if (len < 6)
        return false;

    bool number, lowercase, uppercase = false;

    for (int i = 0; i < len; i++)
    {
        if (password[i] >= 97 && password[i] <= 122)
            lowercase = true;
        else if (password[i] >= 65 && password[i] <= 90)
            uppercase = true;
        else if (password[i] >= 48 && password[i] <= 57)
            number = true;
    }

    if (number && lowercase && uppercase)
        return true;

    return false;
}

void registerUser()
{
    char id[100] = {0}, password[100] = {0};
    read(new_socket, id, 100);
    read(new_socket, password, 100);

    if (!isIdUnique(id))
    {
        sendMessage("The id has been registered. Please use another name.");
        return;
    }

    if (!isPasswordValid(password))
    {
        sendMessage("Password must contain a number, a lowercase, an uppercase, and consist of minimum 6 characters.");
        return;
    }

    registerNewUser(id, password);
    sendMessage("User has successfully registered.");
}

void *executeMakeDirectory(void *arg)
{
    char *filePath = (char *)arg;
    mkdir(filePath, 0777);
}

void makeDirectory(char filePath[])
{
    pthread_t th;
    pthread_create(&th, NULL, &executeMakeDirectory, (void *)filePath);
    pthread_join(th, NULL);
}

void *copyFile(void *arg)
{
    char source[100], target[100] = {0};
    char *args = (char *)arg;

    char *token = strtok(args, "|");
    strcpy(source, token);
    token = strtok(NULL, "\n");
    strcpy(target, token);

    FILE *fptr1, *fptr2;

    fptr1 = fopen(source, "r");
    fptr2 = fopen(target, "w");

    if (fptr1 == NULL)
    {
        perror(source);
        exit(0);
    }
    if (fptr2 == NULL)
    {
        perror(target);
        exit(0);
    }

    char c = fgetc(fptr1);

    while (c != EOF)
    {
        fputc(c, fptr2);
        c = fgetc(fptr1);
    }

    fclose(fptr1);
    fclose(fptr2);
}

void addProblemList(char judul[])
{
    FILE *fp;
    fp = fopen("problems.tsv", "a");

    char problems[512] = {0};
    strcpy(problems, judul);
    strcat(problems, "\t");
    strcat(problems, user.name);
    strcat(problems, "\n");

    fputs(problems, fp);
    fclose(fp);
}

void makeAndCopyFileContent(char judul[], char fileName[])
{

    char filePath[200];
    strcpy(filePath, judul);
    strcat(filePath, "/");
    strcat(filePath, fileName);

    FILE *fptr;

    fptr = fopen(filePath, "w+");
    if (fptr == NULL)
    {
        perror(filePath);
        exit(0);
    }

    while (true)
    {
        char str[200] = {0};
        read(new_socket, str, 200);
        if (strcmp(str, "stop-add-file 123") == 0)
            break;
        fprintf(fptr, "%s", str);
    }

    fclose(fptr);
}

bool isProblemNameAlreadyExists(char judul[])
{
    FILE *fp;
    fp = fopen("problems.tsv", "r");

    char str[200];

    while (fgets(str, 200, fp) != NULL)
    {
        char *token = strtok(str, "\t");

        if (strcmp(token, judul) == 0)
            return true;
    }

    fclose(fp);
    return false;
}

void addProblem()
{
    char judul[100] = {0};
    sendMessage("Judul problem: ");
    read(new_socket, judul, 100);

    if (isProblemNameAlreadyExists(judul))
    {
        sendMessage("problem-exists-1");
        sendMessage("Process failed. Problem already exists. Please try with another name.");
        return;
    }
    else
        sendMessage("problem-exists-0");

    addProblemList(judul);
    makeDirectory(judul);

    sendMessage("Filepath description.txt: ");
    makeAndCopyFileContent(judul, "description.txt");

    sendMessage("Filepath input.txt: ");
    makeAndCopyFileContent(judul, "input.txt");

    sendMessage("Filepath output.txt: ");
    makeAndCopyFileContent(judul, "output.txt");

    sendMessage("Problem has been successfully added.");
}

void seeAllProblems()
{
    FILE *fp;
    fp = fopen("problems.tsv", "r");

    char str[200];

    while (fgets(str, 200, fp) != NULL)
    {
        char problemDetail[200];

        char *token = strtok(str, "\t");
        strcpy(problemDetail, token);
        strcat(problemDetail, " by ");
        token = strtok(NULL, "\n");
        strcat(problemDetail, token);

        sendMessage(problemDetail);
    }
    sendMessage("stop");

    fclose(fp);
}

bool isFilesEqual(char judul[], char answerFile[])
{
    FILE *fp1, *fp2;

    char realAnswerFile[100];
    strcpy(realAnswerFile, judul);
    strcat(realAnswerFile, "/output.txt");

    fp1 = fopen(realAnswerFile, "r");
    fp2 = fopen(answerFile, "r");

    if (fp1 == NULL)
    {
        perror(realAnswerFile);
        exit(0);
    }
    if (fp2 == NULL)
    {
        perror(answerFile);
        exit(0);
    }

    char c1 = fgetc(fp1);
    char c2 = fgetc(fp2);

    while (c1 != EOF && c2 != EOF)
    {
        if (c1 != c2)
            return false;
        c1 = fgetc(fp1);
        c2 = fgetc(fp2);
    }

    fclose(fp1);
    fclose(fp2);
    return true;
}

void submitAnswer()
{
    char judul[100] = {0}, answerFile[100] = {0};

    read(new_socket, judul, 100);
    read(new_socket, answerFile, 100);

    if (isFilesEqual(judul, answerFile))
        sendMessage("AC");
    else
        sendMessage("WA");
}

void *sendProblemContent(void *arg)
{
    char *filePath = (char *)arg;

    FILE *fp;
    fp = fopen(filePath, "r");

    char str[100];

    while (fgets(str, 100, fp) != NULL)
    {
        sendMessage(str);
    }

    sendMessage("file-stop 123"); // mark the file reading is done

    fclose(fp);
}

void sendProblem(char filePath[])
{
    pthread_t th;
    pthread_create(&th, NULL, &sendProblemContent, (void *)filePath);
    pthread_join(th, NULL);
}

void downloadProblem()
{
    char judul[100] = {0};
    read(new_socket, judul, 100);

    char descPath[100] = "./", inputPath[100] = "./";
    strcat(descPath, judul);
    strcat(descPath, "/description.txt");
    strcat(inputPath, judul);
    strcat(inputPath, "/input.txt");

    sendProblem(descPath);
    sendProblem(inputPath);
}

int main(int argc, char const *argv[])
{
    int server_fd, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);

restartPoint:
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    printf("Server successfully connected to client...\n\n");

    close(server_fd);

    FILE *fp;
    if (access("problems.tsv", F_OK) != 0)
    {
        fp = fopen("problems.tsv", "w+");
        fclose(fp);
    }
    if (access("users.txt", F_OK) != 0)
    {
        fp = fopen("users.txt", "w+");
        fclose(fp);
    }

    bool isUserAuthenticated = false;

    while (1)
    {
        char command[10] = {0};
        if (!isUserAuthenticated)
        {

            read(new_socket, command, 10);

            if (strcmp(command, "login") == 0)
            {
                loginUser(&isUserAuthenticated);
            }
            else if (strcmp(command, "register") == 0)
            {
                registerUser();
            }
        }
        else
        {
            read(new_socket, command, 10);

            if (strcmp(command, "add") == 0)
            {
                addProblem();
            }
            else if (strcmp(command, "see") == 0)
            {
                seeAllProblems();
            }
            else if (strcmp(command, "download") == 0)
            {
                downloadProblem();
            }
            else if (strcmp(command, "submit") == 0)
            {
                submitAnswer();
            }
            else if (strcmp(command, "exit") == 0)
                break;
        }
    }

    printf("\nServer is restarting...\n");

    goto restartPoint;

    return 0;
}